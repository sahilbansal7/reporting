package com.nearbuy.reporting.configrations;

import org.springframework.http.HttpStatus;

/**
 * Utility class for Rest related activities 
 * 
 * @author Sunny
 *
 */
public final class RestUtil 
{
	/**
	 * private constructor for utility class
	 */
	private RestUtil()
	{
		
	}
	
	/**
	 * Check the HTTPStatus for error
	 * 
	 * @param status
	 * @return
	 */
	public static boolean isError(final HttpStatus status) 
	{
        final HttpStatus.Series series = status.series();
        return HttpStatus.Series.CLIENT_ERROR.equals(series)
                || HttpStatus.Series.SERVER_ERROR.equals(series);
    }
}
