package com.nearbuy.reporting.configrations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;

/**
 * 
 * @author vaibhavraj
 *
 */
@Configuration
public class RestTemplateConfiguration {
    
    public static final Integer HTTP_REQ_READ_TIMEOUT = 5000;
    /**
     * http connection timeout
     */
    public static final Integer HTTP_REQ_CONNECTION_TIMEOUT = 5000;
    @Bean(name = "restTemplate")
    public RestTemplate restTemplate() {
        final RestTemplate restTemplate = new RestTemplate();
        return restTemplate;
    }
    
    @Bean(name = { "restTemplateWithErrorHandler" })
    public RestTemplate restTemplateWithErrorHandler()
    {
        final RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new FormHttpMessageConverter());
        restTemplate.setErrorHandler(new RestResponseErrorHandler());
        return restTemplate;
    }

    @Bean(name = "taskExecutor")
    public TaskExecutor getTaskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(4);
        taskExecutor.setMaxPoolSize(8);
        taskExecutor.setQueueCapacity(20);
        return taskExecutor;
    }

}
