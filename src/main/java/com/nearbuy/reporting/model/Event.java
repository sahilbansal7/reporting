package com.nearbuy.reporting.model;

import java.util.List;

public class Event {

    private String eventName;
    private List<NotificationSetting> notificationSettings;
    public String getEventName() {
        return eventName;
    }
    public void setEventName(String eventName) {
        this.eventName = eventName;
    }
    public List<NotificationSetting> getNotificationSettings() {
        return notificationSettings;
    }
    public void setNotificationSettings(List<NotificationSetting> notificationSettings) {
        this.notificationSettings = notificationSettings;
    }
    
    
}
