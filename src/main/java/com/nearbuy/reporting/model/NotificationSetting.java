package com.nearbuy.reporting.model;

import java.util.Set;

public class NotificationSetting {

    private String notificationType;
    private Set<String> frequency;
    private Boolean isApplicable;
    public String getNotificationType() {
        return notificationType;
    }
    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }
    public Set<String> getFrequency() {
        return frequency;
    }
    public void setFrequency(Set<String> frequency) {
        this.frequency = frequency;
    }
    public Boolean getIsApplicable() {
        return isApplicable;
    }
    public void setIsApplicable(Boolean isApplicable) {
        this.isApplicable = isApplicable;
    }
    
    
}
