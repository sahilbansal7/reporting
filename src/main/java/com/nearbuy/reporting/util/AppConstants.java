package com.nearbuy.reporting.util;

import java.text.SimpleDateFormat;

public final class AppConstants {

    public static final String MERCHANT = "Merchant";
    public static final String WEEKLY_REPORT = "WEEKLY_REPORT";
    public static final String SUCCESS_MESSAGE = "Success";
    public static final String FAILURE_MESSAGE = "-1";
    public static final String MAIL_TEMPLATE_ID = null;
    public static String DATE_TIME_INPUT_FORMAT = "dd-MM-yyyy";
    public static SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(DATE_TIME_INPUT_FORMAT);    
}
