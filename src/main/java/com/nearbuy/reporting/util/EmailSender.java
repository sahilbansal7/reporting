package com.nearbuy.reporting.util;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.nearbuy.reporting.request.TemplateEmailRequest;
import com.nearbuy.reporting.response.EmailServiceResponseDTO;

@Component
public class EmailSender {

    private Logger logger = LoggerFactory.getLogger(EmailSender.class);

    @Autowired
    private TaskExecutor taskExecutor;

    @Autowired
    @Qualifier("restTemplateWithErrorHandler")
    private RestTemplate restTemplate;

    @Value("${emailService.url}")
    private String emailServiceEndpoint;

    public void sendEmail(List<TemplateEmailRequest> emails) {
        logger.info("Entered EmailSender.sendEmail");
        for (TemplateEmailRequest email : emails) {
            logger.info("Sending email , request : " + email);
            Runnable task = getTask(email);
            taskExecutor.execute(task);
        }
        logger.info("Exiting EmailSender.sendEmail");
    }

    private Runnable getTask(final TemplateEmailRequest merchantEmail) {
        Runnable task = new Runnable() {
            @Override
            public void run() {
                String emailRequest =  new Gson().toJson(merchantEmail);
                HttpHeaders headers = new HttpHeaders();
               // headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_UTF8_VALUE);
                //headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
                // Rest call to email service logic comes here
                HttpEntity<Object> request = new HttpEntity<Object>(emailRequest, headers);
                ResponseEntity<String> response = restTemplate.exchange(
                        emailServiceEndpoint, HttpMethod.POST, request, String.class);
                logger.info("Request : " + request);
                logger.info("Response : " + response);
                EmailServiceResponseDTO emailServiceResponseDTO = new EmailServiceResponseDTO();

                if (response != null) {
                    if (isError(response.getStatusCode())) {
                        logger.error("Error in sending mail with status code "
                                + String.valueOf(response.getStatusCode())
                                + " for date ["
                                + AppConstants.SIMPLE_DATE_FORMAT.format(new Date())
                                + "]");

                        emailServiceResponseDTO.setStatus(AppConstants.FAILURE_MESSAGE);
                    } else {
                        emailServiceResponseDTO.setStatus(AppConstants.SUCCESS_MESSAGE);
                    }
                }
                // Set method exit info
                logger.debug("Exit method - sendEmail");
            }

            public boolean isError(final HttpStatus status) {
                final HttpStatus.Series series = status.series();
                return HttpStatus.Series.CLIENT_ERROR.equals(series)
                        || HttpStatus.Series.SERVER_ERROR.equals(series);
            }
        };
        return task;
    }
}
