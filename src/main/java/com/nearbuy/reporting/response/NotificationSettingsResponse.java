package com.nearbuy.reporting.response;

import java.util.List;

import com.nearbuy.reporting.model.NotificationSetting;

public class NotificationSettingsResponse {

    List<NotificationSetting> notificationSettings;

    public List<NotificationSetting> getNotificationSettings() {
        return notificationSettings;
    }

    public void setNotificationSettings(List<NotificationSetting> notificationSettings) {
        this.notificationSettings = notificationSettings;
    }
    
    
}
