package com.nearbuy.reporting.response;

public class EmailServiceResponseDTO {

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "EmailServiceResponseDTO [status=" + status + "]";
    }

}
