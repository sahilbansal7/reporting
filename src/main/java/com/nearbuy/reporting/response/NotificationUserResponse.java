    package com.nearbuy.reporting.response;

import java.io.Serializable;
import java.util.List;

import com.nearbuy.reporting.db.model.UserPreference;

public class NotificationUserResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    List<UserPreference> userPreferences;

    public List<UserPreference> getUserPreferences() {
        return userPreferences;
    }

    public void setUserPreferences(List<UserPreference> userPreferences) {
        this.userPreferences = userPreferences;
    }

  
    

}
