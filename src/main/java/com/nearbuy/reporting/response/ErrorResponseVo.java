package com.nearbuy.reporting.response;

import java.io.Serializable;


public class ErrorResponseVo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4405584636929049404L;
	private String message;
	
	public ErrorResponseVo(String message){
		this.message = message;
	
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessage() {
		return message;
	}

}
