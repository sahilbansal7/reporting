package com.nearbuy.reporting.response;

import java.util.List;

import com.nearbuy.reporting.db.model.NotificationEvent;

public class EventResponse {

    List<NotificationEvent> events;

    public List<NotificationEvent> getEvents() {
        return events;
    }

    public void setEvents(List<NotificationEvent> events) {
        this.events = events;
    }
    
    
}
