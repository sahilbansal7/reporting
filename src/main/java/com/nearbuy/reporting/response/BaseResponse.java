package com.nearbuy.reporting.response;

public class BaseResponse {

    public BaseResponse() {
        super();
    }

    private String msg;

    public BaseResponse(String msg) {
        super();
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "BaseResponse [msg=" + msg + "]";
    }

}
