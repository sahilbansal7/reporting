package com.nearbuy.reporting.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.nearbuy.reporting.db.model.UserPreference;
import com.nearbuy.reporting.model.Event;
import com.nearbuy.reporting.model.NotificationSetting;
import com.nearbuy.reporting.request.UpdateUserRequest;
import com.nearbuy.reporting.response.BaseResponse;
import com.nearbuy.reporting.response.NotificationSettingsResponse;
import com.nearbuy.reporting.response.NotificationUserResponse;
import com.nearbuy.reporting.scheduler.NotificationPreferenceScheduler;

/**
 * Created by Sahil on 20/11/15.
 */
@RestController
public class UserPreferenceController {

    private static final Logger logger = LoggerFactory.getLogger(UserPreferenceController.class);

    @Autowired
    private NotificationPreferenceScheduler notificationService;

    @RequestMapping(value = "/v1/userPreferences", method = RequestMethod.GET)
    public @ResponseBody UserPreference getUser() {
        logger.info("Entered in NotificationController:getNotificationUser");
        notificationService.storeNotificationPreference();
        return null;

    }

    @RequestMapping(value = "/v1/userPreferences/{userType}/{userId}", method = RequestMethod.PUT, consumes = "application/json")
    public @ResponseBody BaseResponse updateNotificationUser(@PathVariable(value = "userId") Long userId, @PathVariable(value = "userType") String userType,
            @RequestBody UpdateUserRequest updateUserRequest) {
        logger.info("Entered in NotificationController:updateNotificationUser");
        BaseResponse baseResponse = null;
        return baseResponse;
  
    }

    @RequestMapping(value = "/v1/userPreferences", method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody BaseResponse addNotificationUser(@RequestBody UserPreference userPreference) {
        logger.info("Entered in NotificationController:addNotificationUser");
        BaseResponse baseResponse = null;
        return baseResponse;

    }

    @RequestMapping(value = "/v1/userPreferences/{userType}/{userId}", method = RequestMethod.POST)
    public @ResponseBody BaseResponse deleteNotificationUser(@PathVariable(value = "userId") Long userId, @PathVariable(value = "userType") String userType,
            @RequestParam(value = "updatedBy") String updatedBy) {
        logger.info("Entered in NotificationController:deleteNotificationUser");
        BaseResponse baseResponse = null;
        return baseResponse;

    }

    @RequestMapping(value = "/v1/userPreferences/{userType}/notification", method = RequestMethod.GET)
    public @ResponseBody NotificationUserResponse getNotificationUsersByUserTypeAndNotificationType(@PathVariable(value = "userType") String userType,
            @RequestParam(value = "notificationType") String notificationType) {
        logger.info("Entered in NotificationController:getNotificationUsers");
        NotificationUserResponse notificationUserResponse = null;
        return notificationUserResponse;

    }

    @RequestMapping(value = "/v1/userPreferences/{userType}", method = RequestMethod.GET)
    public @ResponseBody NotificationUserResponse getNotificationUsersByUserTypeAndEventType(@PathVariable(value = "userType") String userType, @RequestParam(
            value = "eventType") String eventType) {
        logger.info("Entered in NotificationController:getNotificationUsers");
        NotificationUserResponse notificationUserResponse = null;
        return notificationUserResponse;

    }

    @RequestMapping(value = "/v1/userPreferences/{userType}/{userId}/events", method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody BaseResponse addEvent(@PathVariable(value = "userId") Long userId, @PathVariable(value = "userType") String userType,
            @RequestBody Event event, @RequestParam String updatedBy) {
        logger.info("Entered in NotificationController:addEvent");
        BaseResponse baseResponse = null;
        return baseResponse;

    }

    @RequestMapping(value = "/v1/userPreferences/{userType}/{userId}/events", method = RequestMethod.DELETE)
    public @ResponseBody BaseResponse deleteEvent(@PathVariable(value = "userId") Long userId, @PathVariable(value = "userType") String userType,
            @RequestParam String eventName, @RequestParam String updatedBy) {
        logger.info("Entered in NotificationController:deleteEvent");
        BaseResponse baseResponse = null;
        return baseResponse;

    }

    /*
     * @RequestMapping(value = "/v1/userPreferences/{userType}/{userId}/events", method = RequestMethod.PUT, consumes =
     * "application/json")
     * public @ResponseBody BaseResponse updateEvents(@PathVariable(value = "userId") Long userId,@PathVariable(value =
     * "userType") String userType,@RequestBody UpdateEventsRequest updateEventsRequest) {
     * logger.info("Entered in NotificationController:updateEvents");
     * BaseResponse baseResponse = notificationService.updateEvents(userType,userId,updateEventsRequest);
     * return baseResponse;
     * 
     * }
     */
    @RequestMapping(value = "/v1/userPreferences/{userType}/{userId}/events", method = RequestMethod.PUT, consumes = "application/json")
    public @ResponseBody BaseResponse updateEvent(@PathVariable(value = "userId") Long userId, @PathVariable(value = "userType") String userType,
            @RequestBody Event event, @RequestParam String updatedBy) {
        logger.info("Entered in NotificationController:updateEvent");
        BaseResponse baseResponse =null;// notificationService.updateEvent(userType, userId, event, updatedBy);
        return baseResponse;

    }

    @RequestMapping(value = "/v1/userPreferences/{userType}/{userId}/events", method = RequestMethod.GET)
    public @ResponseBody NotificationSettingsResponse getNotificationSettings(@PathVariable(value = "userId") Long userId,
            @PathVariable(value = "userType") String userType, @RequestParam(value = "eventName") String eventName) {
        logger.info("Entered in NotificationController:getNotificationSettings");
        NotificationSettingsResponse notificationSettingsResponse  =null;//  notificationService.getNotificationSettings(userType, userId, eventName);
        return notificationSettingsResponse;

    }

    @RequestMapping(value = "/v1/userPreferences/{userType}/{userId}/{eventName}/notificationSetting", method = RequestMethod.GET)
    public @ResponseBody NotificationSetting getNotificationSetting(@PathVariable(value = "userId") Long userId,
            @PathVariable(value = "userType") String userType, @PathVariable(value = "eventName") String eventName,
            @RequestParam(value = "noticationType") String notificationType) {
        logger.info("Entered in NotificationController:getNotificationSetting");
        NotificationSetting notificationSetting  =null;// notificationService.getNotificationSetting(userType, userId, eventName, notificationType);
        return notificationSetting;

    }

    @RequestMapping(value = "/v1/userPreferences/{userType}/{userId}/{eventName}/notificationSetting", method = RequestMethod.PUT, consumes = "application/json")
    public @ResponseBody BaseResponse setNotificationSetting(@PathVariable(value = "userId") Long userId,
            @PathVariable(value = "userType") String userType, @PathVariable(value = "eventName") String eventName,
            @RequestParam(value = "notificationType") String notificationType,  @RequestParam(value = "updatedBy") String updatedBy, @RequestBody NotificationSetting notificationSetting) {
        logger.info("Entered in NotificationController:setNotificationSetting");
        BaseResponse baseResponse  =null;//  notificationService.setNotificationSetting(userType, userId, eventName, notificationType,notificationSetting,updatedBy);
        return baseResponse;

    }
}
