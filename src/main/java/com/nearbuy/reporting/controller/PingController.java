package com.nearbuy.reporting.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PingController {

    private Logger logger = LoggerFactory.getLogger(PingController.class);

    @RequestMapping(value = "/ping", method = { RequestMethod.GET })
    private String ping() {
        logger.info("Welcome to Notification Preference");
        return "pong";
    }

}
