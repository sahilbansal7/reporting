package com.nearbuy.reporting.facade;

import org.springframework.stereotype.Service;

@Service
public interface NotificationPreferenceFacade {
    
    public Boolean storeNotificationPreference();

}
