package com.nearbuy.reporting.facade;

import org.springframework.stereotype.Service;

@Service
public interface WeeklyMailerFacade {

     public void sendMailer();

}
