package com.nearbuy.reporting.facade.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nearbuy.reporting.db.model.ReportingUser;
import com.nearbuy.reporting.db.repository.ReportingUserRepository;
import com.nearbuy.reporting.facade.WeeklyMailerFacade;
import com.nearbuy.reporting.request.TemplateEmailRequest;
import com.nearbuy.reporting.util.AppConstants;
import com.nearbuy.reporting.util.AppUtil;

@Component
public class WeeklyMailerFacadeImpl implements WeeklyMailerFacade {
    
    @Autowired
    private ReportingUserRepository reportingUserRepository;
    
    @Override
    public void sendMailer(){
        List<ReportingUser> users = reportingUserRepository.findUsersByDate(AppUtil.getDate());
        if(users != null){
            for(ReportingUser user:users){
                List<String> emails = new ArrayList<String>();
                emails.add(user.getEmailId());
                createMailRequest(emails,null,user.getPlaceHolderMap());
            }
        }
        
    }
    private TemplateEmailRequest createMailRequest(List<String> emails, List<String> ccTo, Map<String, String> map) {
        TemplateEmailRequest emailRequest = new TemplateEmailRequest();
        emailRequest.setEmailTo(emails);
        emailRequest.setCcTo(ccTo);
        emailRequest.setIsHtml(Boolean.TRUE);
        emailRequest.setPlaceHolderMap(map);
        emailRequest.setTemplateId(AppConstants.MAIL_TEMPLATE_ID);
        return emailRequest;
    }
}
