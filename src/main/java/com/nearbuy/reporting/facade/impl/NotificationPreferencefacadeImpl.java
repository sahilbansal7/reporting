package com.nearbuy.reporting.facade.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.nearbuy.reporting.db.model.ReportingUser;
import com.nearbuy.reporting.db.repository.ReportingUserRepository;
import com.nearbuy.reporting.facade.NotificationPreferenceFacade;
import com.nearbuy.reporting.model.AccountUser;
import com.nearbuy.reporting.model.NotificationSetting;
import com.nearbuy.reporting.service.AccountService;
import com.nearbuy.reporting.service.NotificationService;
import com.nearbuy.reporting.util.AppUtil;

@Component
public class NotificationPreferencefacadeImpl implements NotificationPreferenceFacade {

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private ReportingUserRepository reportingUserRepository;

    @Override
    public Boolean storeNotificationPreference() {

        List<Long> accountIds = accountService.getAllAccountIds();
        Boolean isPlaceHolderNeeded = Boolean.TRUE;
       // for (Long accountId : accountIds) {
            List<AccountUser> accountUsers = accountService.getUsersByAccountId(1001760l);
            for (AccountUser accountUser : accountUsers) {
                NotificationSetting notificationSetting = notificationService.getNotificationSettingById(accountUser.getId());
                HashMap<String, String> placeHolderMap = new HashMap<String, String>();
                if (notificationSetting == null || notificationSetting.getIsApplicable() != null && notificationSetting.getIsApplicable().equals(Boolean.TRUE)) {
                    ReportingUser user = new ReportingUser();
                    user.setUserId(accountUser.getId());
                    user.setEmailId(accountUser.getEmail());
                    user.setDate(AppUtil.getDate());
                    if (isPlaceHolderNeeded) {
                        placeHolderMap = getPlaceHolders(1001760l);
                        isPlaceHolderNeeded = Boolean.FALSE;
                    }
                    user.setPlaceHolderMap(placeHolderMap);
                    
                    
                    System.out.println("user : "+ user);
                    
                    try {
                        ReportingUser user_ = reportingUserRepository.save(user);
                        
                        
                        System.out.println("user_ : "+ user_);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    
                }
            }
       // }
        return Boolean.TRUE;
    }

    private HashMap<String, String> getPlaceHolders(Long accountId) {
        HashMap<String, String> placeHolderMap = new HashMap<String, String>();

        return placeHolderMap;
    }

}
