package com.nearbuy.reporting.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "The Input Request is not correct")
public class InvalidRequestException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public InvalidRequestException() {
        super();

    }

    public InvalidRequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
      
    }

    public InvalidRequestException(String message, Throwable cause) {
        super(message, cause);
       
    }

    public InvalidRequestException(String message) {
        super(message);
            }

    public InvalidRequestException(Throwable cause) {
        super(cause);
    }

    
    
    
}
