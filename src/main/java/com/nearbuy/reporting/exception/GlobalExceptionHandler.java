package com.nearbuy.reporting.exception;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.nearbuy.reporting.response.ErrorResponseVo;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(DataNotFoundException.class)
    public @ResponseBody ErrorResponseVo handleDataNotFoundException(HttpServletRequest request, Exception ex, HttpServletResponse httpServletResponse) {
        ErrorResponseVo errorResponseVo = new ErrorResponseVo(ex.getMessage());
        return errorResponseVo;
    }

    @ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "UserName or Password is incorrect")
    @ExceptionHandler(InvalidCredentialsException.class)
    public @ResponseBody ErrorResponseVo handleInvalidCredentialsException(HttpServletRequest request, Exception ex) {
        ErrorResponseVo errorResponseVo = new ErrorResponseVo(ex.getMessage());
        return errorResponseVo;
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(InvalidRequestException.class)
    public @ResponseBody ErrorResponseVo handleInvalidRequestException(HttpServletRequest request, Exception ex) {
        logger.error("Exception : " + ex);

        ErrorResponseVo errorResponseVo = new ErrorResponseVo(ex.getMessage());

        return errorResponseVo;
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler({ MethodArgumentNotValidException.class, HttpMessageNotReadableException.class })
    public @ResponseBody ErrorResponseVo handleValidationException(HttpServletRequest request, Exception ex) {
        logger.error("Exception : ", ex);
        if (ex instanceof HttpMessageNotReadableException) {
            return new ErrorResponseVo("The request sent is either empty or invalid");
        }
        MethodArgumentNotValidException exception = (MethodArgumentNotValidException) ex;
        List<FieldError> field = exception.getBindingResult().getFieldErrors();
        List<String> mandatoryfields = new ArrayList<String>();
        for (FieldError fe : field) {
            mandatoryfields.add(fe.getField());
        }
        ErrorResponseVo errorResponseVo = new ErrorResponseVo("Following mandatory fields are missing " + mandatoryfields.toString());
        return errorResponseVo;
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public @ResponseBody ErrorResponseVo handleException(HttpServletRequest request, Exception ex) {
        logger.error("Exception : ", ex);
        ex.printStackTrace();
        ErrorResponseVo errorResponseVo = new ErrorResponseVo("We are facing some technical Issues . Please try after some time");
        return errorResponseVo;
    }
}
