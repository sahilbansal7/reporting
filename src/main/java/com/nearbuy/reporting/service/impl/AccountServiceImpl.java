package com.nearbuy.reporting.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.nearbuy.reporting.model.AccountUser;
import com.nearbuy.reporting.service.AccountService;
@Component
public class AccountServiceImpl implements AccountService {
    
    @Autowired
    @Qualifier("restTemplateWithErrorHandler")
    RestTemplate restTemplate;
    private Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

  

    @Value("${api.endpoint.account}")
    private String endPointUrl;
    @Value("${api.endpoint.account}")
    private String accountUserendPointUrl;
    @Override
    public List<Long> getAllAccountIds() {
        logger.info("Entered in AccountUserServiceImpl.getAccountByEmail");
        String uri = endPointUrl;
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(headers);
        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(uri, HttpMethod.POST, request, String.class);
            if (HttpStatus.OK == response.getStatusCode()) {
                String responseMsg = response.getBody();
                List<Long> json = new Gson().fromJson(responseMsg, new TypeToken<List<Long>>() {
                }.getType());
                logger.info("Sucess Response for getAccountByEmail ");
                return json;
            }
        } catch (Exception e) {
            logger.error("Exception ocurred while getting account for email:");
            e.printStackTrace();
        }

        return null;
    }
    @Override
    public List<AccountUser> getUsersByAccountId(Long accountId) {
        logger.info("Entered in AccountUserServiceImpl.getUsersByAccountId");
        String uri = accountUserendPointUrl + accountId + "/users";
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(headers);
        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(uri, HttpMethod.GET, request, String.class);
            if (HttpStatus.OK == response.getStatusCode()) {
                String responseMsg = response.getBody();
                List<AccountUser> json = new Gson().fromJson(responseMsg, new TypeToken<List<AccountUser>>() {
                }.getType());
                logger.info("Sucess Response for getUsersByAccountId ", accountId);
                return json;
            }
        } catch (Exception e) {
            logger.error("Exception ocurred while getUsersByAccountId for accountid:" + accountId);
            e.printStackTrace();
        }

        return null;
    }
  
}
