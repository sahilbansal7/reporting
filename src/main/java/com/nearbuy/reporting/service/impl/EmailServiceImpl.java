package com.nearbuy.reporting.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.nearbuy.reporting.request.EmailServiceRequestDTO;
import com.nearbuy.reporting.response.EmailServiceResponseDTO;
import com.nearbuy.reporting.service.EmailService;

@Component
public class EmailServiceImpl implements EmailService {

    private Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);

    @Autowired
    @Qualifier("restTemplateWithErrorHandler")
    private RestTemplate template;

    @Value("${emailService.url}")
    private String sendMailEndPointUrl;

    @Override
    public EmailServiceResponseDTO sendEmail(
            EmailServiceRequestDTO emailServiceRequestDTO) {
        // Step1: Set method entered info
        logger.debug("Entered method - sendEmail");
        MultiValueMap<String, Object> multiValueMap = new LinkedMultiValueMap<String, Object>();
        for (String emailTo : emailServiceRequestDTO.getEmailTo()) {
            multiValueMap.add("emailTo", emailTo);
        }
        if (emailServiceRequestDTO.getCcTo() != null)
            for (String ccTo : emailServiceRequestDTO.getCcTo()) {
                multiValueMap.add("ccTo", ccTo);
            }
        if (emailServiceRequestDTO.getBccTo() != null)
            for (String bccTo : emailServiceRequestDTO.getBccTo()) {
                multiValueMap.add("bccTo", bccTo);
            }
        multiValueMap.add("emailFrom", emailServiceRequestDTO.getEmailfrom());
        multiValueMap.add("subject", emailServiceRequestDTO.getSubject());
        multiValueMap.add("emailBody", emailServiceRequestDTO.getEmailBody());
        multiValueMap.add("isHtml", true);
        if (emailServiceRequestDTO.getAttachments() != null) {
            for (Resource attachment : emailServiceRequestDTO.getAttachments()) {
                multiValueMap.add("multiPartFileData", attachment);
            }
        }
        // Step2::
        // Create send mail header
        HttpHeaders headers = new HttpHeaders();
        headers.set("multipart", "form-data");
        // Step3::
        // Create request for sending mail api
        HttpEntity<Object> request = new HttpEntity<Object>(multiValueMap, headers);
        ResponseEntity<String> response = template.exchange(
                sendMailEndPointUrl, HttpMethod.POST, request, String.class);
        EmailServiceResponseDTO emailServiceResponseDTO = new EmailServiceResponseDTO();

        if (response != null) {
            if (isError(response.getStatusCode())) {
                logger.error("Error in sending mail with status code "
                        + String.valueOf(response.getStatusCode())
                        + "]");

                emailServiceResponseDTO.setStatus("Failure");
            } else {
                emailServiceResponseDTO.setStatus("Success");
            }
        }

        // Set method exit info
        logger.debug("Exit method - sendEmail");
        return emailServiceResponseDTO;
    }

    public boolean isError(final HttpStatus status) {
        final HttpStatus.Series series = status.series();
        return HttpStatus.Series.CLIENT_ERROR.equals(series)
                || HttpStatus.Series.SERVER_ERROR.equals(series);
    }

}
