package com.nearbuy.reporting.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.nearbuy.reporting.model.NotificationSetting;
import com.nearbuy.reporting.service.NotificationService;
import com.nearbuy.reporting.util.AppConstants;
@Service
public class NotificationServiceImpl implements NotificationService {
    
    @Autowired
    @Qualifier("restTemplateWithErrorHandler")
    RestTemplate restTemplate;

    @Value("${api.endpoint.notification}")
    private String endPointUri;

    private Logger logger = LoggerFactory.getLogger(NotificationServiceImpl.class);
    
    @Override
    public NotificationSetting getNotificationSettingById(Long userId) {
        logger.info("Entered in NotificationFacadeImpl.getNotificationSettingById with userId : " + userId);
        String uri = endPointUri + AppConstants.MERCHANT + "/" + userId + "/" + "VOUCHER_REDEMPTION"+ "/notificationSetting?noticationType=EMAIL";
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(headers);
        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(uri, HttpMethod.GET, request, String.class);
            if (HttpStatus.OK == response.getStatusCode()) {
                String responseMsg = response.getBody();
                NotificationSetting json = new Gson().fromJson(responseMsg, NotificationSetting.class);
                logger.info("Success Response for notification Setting for userId :", userId);
                return json;
            }
        } catch (Exception e) {
            logger.error("Exception ocurred while getting Notification setting for userId:" + userId);
            e.printStackTrace();
        }

        return null;
    }
    

    
    /*
    public UserPreference getUserPreference(Long userId, String userType) {
        UserPreference userPreference = new UserPreference();
        if (userId == null || userType == null) {
            return userPreference;
        }
        userPreference = userPreferenceRepository.findUserbyUserTypeanduserId(userType, userId);
        return userPreference;
    }

    public BaseResponse addUser(UserPreference userPreference) {

        if (userPreference == null || userPreference.getUserId() == null || StringUtils.isBlank(userPreference.getUserType())
                || StringUtils.isBlank(userPreference.getCreatedBy()) || userPreference.getEvents() == null) {
            throw new InvalidRequestException("Invalid request as either of the following does not exist: userId,userType,createdBy or events ");
        }
        UserPreference notificationUser = userPreferenceRepository.findUserbyUserTypeanduserId(userPreference.getUserType(), userPreference.getUserId());
        if (notificationUser != null) {
            throw new InvalidRequestException("User already exist for userId:" + userPreference.getUserId() + " and UserType:" + userPreference.getUserType());
        }
        userPreference.setCreatedBy(userPreference.getCreatedBy());
        userPreference.setCreatedAt(System.currentTimeMillis());
        userPreference.setLastUpdatedBy(userPreference.getCreatedBy());
        userPreference.setLastUpdatedAt(userPreference.getCreatedAt());
        userPreference.setIsDeleted(Boolean.FALSE);
        if (userPreference.getIsDND() == null) {
            userPreference.setIsDND(Boolean.FALSE);
        }

        eventUtil.validateEvents(userPreference.getEvents(), userPreference.getUserType());

        EventResponse eventResponse = eventService.getEventsforUserType(userPreference.getUserType());
        Set<String> events = new HashSet<String>();
        for (NotificationEvent event : eventResponse.getEvents()) {
            events.add(event.getEventName());
        }

        if (userPreference.getEvents() == null || userPreference.getEvents().size() == 0) {
            throw new InvalidRequestException("No events are defined in the request");
        }

        for (Event event : userPreference.getEvents()) {
            if (events.contains(event.getEventName())) {

                if (event.getNotificationSettings() == null) {
                    event.setNotificationSettings(eventService.getDefaultNotificationSetting(event.getEventName(), userPreference.getUserType()));
                }
            }
            else {
                throw new InvalidRequestException("EventName " + event.getEventName() + " for userType " + userPreference.getUserType()
                        + " does not exist in  the system");
            }
        }

        userPreferenceRepository.save(userPreference);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setMsg("User added Successfully");
        return baseResponse;
    }

    public BaseResponse updateUser(Long userId, String userType, UpdateUserRequest updateUserRequest) {
        if(updateUserRequest == null || StringUtils.isBlank(updateUserRequest.getUpdatedBy())){
            throw new InvalidRequestException("UpdatedBy is not present in the request");
        }
        UserPreference userPreference = getUserPreference(userId, userType);
        if (userPreference == null) {
            throw new DataNotFoundException("User for user Id " + userId + " and userType " + userType + " does not exist");
        }
        if (updateUserRequest.getIsDND() != null) {
            userPreference.setIsDND(updateUserRequest.getIsDND());
            userPreference.setLastUpdatedBy(updateUserRequest.getUpdatedBy());
            userPreference.setLastUpdatedAt(System.currentTimeMillis());
        }

        userPreferenceRepository.save(userPreference);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setMsg("User Updated successfully");
        return baseResponse;
    }

    public NotificationUserResponse getUsersByNotificationTypeAndUserType(String userType, String notificationType) {
        NotificationUserResponse notificationUserResponse = new NotificationUserResponse();
        List<UserPreference> userPreferences = userPreferenceRepository.findUsersbyNotificationTypeAndUserType(notificationType, userType);
        notificationUserResponse.setUserPreferences(userPreferences);
        return notificationUserResponse;
    }

    public BaseResponse addEvent(String userType, Long userId, Event event, String updatedBy) {
        if (userType == null || userId == null || event == null) {
            throw new InvalidRequestException("Atleast one of the required parameter is null");
        }

        UserPreference userPreference = userPreferenceRepository.findUserbyUserTypeanduserId(userType, userId);
        if (userPreference == null) {
            throw new InvalidRequestException("User does not exist for userId:" + userId + " and UserType:" + userType);
        }
        List<Event> events = new ArrayList<Event>();
        events.add(event);
        eventUtil.validateEvents(events, userType);
        Boolean status = userPreferenceRepository.addEventFromUserPreference(userType, userId, event, updatedBy);
        BaseResponse baseResponse = new BaseResponse();
        if (status.equals(Boolean.TRUE)) {
            baseResponse.setMsg("Event Successfully added for the user");
        }
        return baseResponse;
    }

    public NotificationSettingsResponse getNotificationSettings(String userType, Long userId, String eventName) {

        UserPreference userPreference = getUserPreference(userId, userType);
        List<NotificationSetting> notificationSettings = new ArrayList<NotificationSetting>();
        for (Event event : userPreference.getEvents()) {
            if (event.getEventName().equalsIgnoreCase(eventName)) {
                notificationSettings = event.getNotificationSettings();
            }
        }
        NotificationSettingsResponse notificationSettingsResponse = new NotificationSettingsResponse();
        notificationSettingsResponse.setNotificationSettings(notificationSettings);
        return notificationSettingsResponse;
    }

    public NotificationSetting getNotificationSetting(String userType, Long userId, String eventName, String notificationType) {

        UserPreference userPreference = getUserPreference(userId, userType);
        if (userPreference != null && userPreference.getEvents() != null)
            for (Event event : userPreference.getEvents()) {
                if (event.getEventName().equalsIgnoreCase(eventName)) {
                    List<NotificationSetting> notificationSettings = event.getNotificationSettings();
                    for (NotificationSetting notificationSetting : notificationSettings) {
                        if (notificationType.equalsIgnoreCase(notificationSetting.getNotificationType())) {
                            return notificationSetting;
                        }
                    }
                }
            }
        return null;
    }

    public BaseResponse deleteUser(String userType, Long userId, String updatedBy) {
        UserPreference userPreference = getUserPreference(userId, userType);
        if (userPreference != null) {
            userPreference.setIsDeleted(Boolean.TRUE);
            userPreference.setLastUpdatedBy(updatedBy);
            userPreferenceRepository.save(userPreference);
        }
        else {
            throw new DataNotFoundException("User for user Id " + userId + " and userType " + userType + " does not exist");
        }
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setMsg("User Deleted Successfully");
        return baseResponse;
    }

    public BaseResponse updateEvents(String userType, Long userId, UpdateEventsRequest updateEventsRequest) {

        if (userType == null || userId == null || updateEventsRequest == null) {
            throw new InvalidRequestException("Atleast one of the required parameter(userType, userId or updateEventsRequest) is null");
        }
        UserPreference userPreference = userPreferenceRepository.findUserbyUserTypeanduserId(userType, userId);
        if (userPreference == null) {
            throw new InvalidRequestException("User does not exist for userId:" + userId + " and UserType:" + userType);
        }
        eventUtil.validateEvents(updateEventsRequest.getEvents(), userType);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setMsg("Events updated successfully");
        return baseResponse;
    }

    public BaseResponse deleteEvent(String userType, Long userId, String eventName, String updatedBy) {
        if (userType == null || userId == null || eventName == null) {
            throw new InvalidRequestException("Atleast one of the required parameter is null");
        }

        UserPreference userPreference = userPreferenceRepository.findUserbyUserTypeanduserId(userType, userId);
        if (userPreference == null) {
            throw new InvalidRequestException("User does not exist for userId:" + userId + " and UserType:" + userType);
        }
        List<Event> events = new ArrayList<Event>();
        Event event = new Event();
        event.setEventName(eventName);
        events.add(event);
        eventUtil.validateEvents(events, userType);
        Boolean status = userPreferenceRepository.deleteEventFromUserPreference(userType, userId, eventName, updatedBy);
        BaseResponse baseResponse = new BaseResponse();
        if (status.equals(Boolean.TRUE)) {
            baseResponse.setMsg("Event Deleted Successfully for the user");
        }
        return baseResponse;
    }

    public BaseResponse updateEvent(String userType, Long userId, Event event, String updatedBy) {
        if (userType == null || userId == null || event == null) {
            throw new InvalidRequestException("Atleast one of the required parameter is null");
        }

        UserPreference userPreference = userPreferenceRepository.findUserbyUserTypeanduserId(userType, userId);
        if (userPreference == null) {
            throw new InvalidRequestException("User does not exist for userId:" + userId + " and UserType:" + userType);
        }
        List<Event> events = new ArrayList<Event>();
        events.add(event);
        eventUtil.validateEvents(events, userType);
        Boolean status = userPreferenceRepository.deleteEventFromUserPreference(userType, userId, event.getEventName(), updatedBy);
        if (status.equals(Boolean.TRUE)) {
            status = userPreferenceRepository.addEventFromUserPreference(userType, userId, event, updatedBy);
        }
        BaseResponse baseResponse = new BaseResponse();
        if (status.equals(Boolean.TRUE)) {
            baseResponse.setMsg("Event Successfully updated for the user");
        }
        else {
            throw new DataNotFoundException("Mongo db error");
        }
        return baseResponse;

    }

    public NotificationUserResponse getUsersByEventTypeAndUserType(String userType, String eventType) {
        NotificationUserResponse notificationUserResponse = new NotificationUserResponse();
        List<UserPreference> userPreferences = userPreferenceRepository.findUsersbyEventTypeAndUserType(eventType, userType);
        notificationUserResponse.setUserPreferences(userPreferences);
        return notificationUserResponse;
    }

    public BaseResponse setNotificationSetting(String userType, Long userId, String eventName, String notificationType, NotificationSetting notificationSetting,String updatedBy) {
        UserPreference userPreference = userPreferenceRepository.findUserbyUserTypeUserIdAndEventName(userType, userId,eventName);
        Boolean status = Boolean.FALSE;
        if (userPreference == null) {
            throw new InvalidRequestException("User or event does not exist for userId:" + userId + " and UserType:" + userType);
        }
        NotificationSetting oldNotificationSetting =  getNotificationSetting(userType, userId, eventName, notificationType);
        if(oldNotificationSetting == null){
           status =  userPreferenceRepository.addNotificationSettingForEvent(userType, userId, eventName, notificationSetting, updatedBy);
        }
        else{
            status =  userPreferenceRepository.deleteNotificationSettingForEvent(userType, userId, eventName, notificationSetting, updatedBy,notificationType);
            status =  userPreferenceRepository.addNotificationSettingForEvent(userType, userId, eventName, notificationSetting, updatedBy);
        }
     BaseResponse baseResponse = null;
     if(status.equals(Boolean.TRUE)){
         baseResponse = new BaseResponse();
         baseResponse.setMsg("Request Processed Successfully");
     }
        return baseResponse;
    }*/

}
