package com.nearbuy.reporting.service;

import org.springframework.stereotype.Service;

import com.nearbuy.reporting.request.EmailServiceRequestDTO;
import com.nearbuy.reporting.response.EmailServiceResponseDTO;
@Service
public interface EmailService {

   public EmailServiceResponseDTO sendEmail(EmailServiceRequestDTO emailServiceRequestDTO);

}
