package com.nearbuy.reporting.service;

import org.springframework.stereotype.Component;

import com.nearbuy.reporting.model.NotificationSetting;
@Component
public interface NotificationService {

   public NotificationSetting getNotificationSettingById(Long userId);

}
