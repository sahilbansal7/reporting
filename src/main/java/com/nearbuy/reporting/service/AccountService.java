package com.nearbuy.reporting.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.nearbuy.reporting.model.AccountUser;

@Component
public interface AccountService {
    
    public List<Long> getAllAccountIds();

   public List<AccountUser> getUsersByAccountId(Long accountId);

}
