package com.nearbuy.reporting.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.nearbuy.reporting.facade.WeeklyMailerFacade;

@Configuration
@EnableScheduling
public class WeeklyMailerScheduler {
    private static final Logger logger = LoggerFactory.getLogger(WeeklyMailerScheduler.class);

    private static final String cronExpression = "0 0 15 * * *";
    
    @Autowired
    private WeeklyMailerFacade weeklyMailerFacade;

   

    //@Scheduled(cron = cronExpression)
    @Scheduled(initialDelay = 120, fixedDelay = 12*3600000)
    public void storeNotificationPreference() {
        logger.info("storeNotificationPreference");
        try {
            weeklyMailerFacade.sendMailer();
        }catch(Exception e){
            
        }
        }

}
