package com.nearbuy.reporting.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.nearbuy.reporting.controller.UserPreferenceController;
import com.nearbuy.reporting.facade.NotificationPreferenceFacade;


@Configuration
@EnableScheduling
public class NotificationPreferenceScheduler {
    private static final Logger logger = LoggerFactory.getLogger(NotificationPreferenceScheduler.class);

    private static final String cronExpression = "0 0 15 * * *";
    
    @Autowired
    private NotificationPreferenceFacade notificationPreferenceFacade;

   

    //@Scheduled(cron = cronExpression)
    @Scheduled(initialDelay = 1298766660, fixedDelay = 12*3600000)
    public void storeNotificationPreference() {
        System.out.println("fadsgdhjk");
        logger.info("storeNotificationPreference");
        try {
            notificationPreferenceFacade.storeNotificationPreference();
        
        }catch(Exception e){
            
        }
        }

}
