package com.nearbuy.reporting.db.model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.nearbuy.reporting.model.NotificationSetting;

@Document(collection = "notification_event")
public class NotificationEvent {
    private String _id;
    private String eventName;
    private String userType;
    private List<NotificationSetting> defaultNotificationSettings;
    public String getEventName() {
        return eventName;
    }
    public void setEventName(String eventName) {
        this.eventName = eventName;
    }
    public List<NotificationSetting> getDefaultNotificationSettings() {
        return defaultNotificationSettings;
    }
    public void setDefaultNotificationSettings(List<NotificationSetting> defaultNotificationSettings) {
        this.defaultNotificationSettings = defaultNotificationSettings;
    }
    public String getUserType() {
        return userType;
    }
    public void setUserType(String userType) {
        this.userType = userType;
    }
    
    
}
