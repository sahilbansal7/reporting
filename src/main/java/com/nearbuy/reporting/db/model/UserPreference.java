package com.nearbuy.reporting.db.model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.nearbuy.reporting.model.Event;

@Document(collection = "reporting")
public class UserPreference {
    
private String _id;
private Long userId;
private String userType;
private Boolean isDND;
private Long lastUpdatedAt;
private String lastUpdatedBy;
private List<Event> events;
private Boolean isDeleted;
private String createdBy;
private Long createdAt;
public String get_id() {
    return _id;
}
public void set_id(String _id) {
    this._id = _id;
}

public Long getUserId() {
    return userId;
}
public void setUserId(Long userId) {
    this.userId = userId;
}
public String getUserType() {
    return userType;
}
public void setUserType(String userType) {
    this.userType = userType;
}
public Boolean getIsDND() {
    return isDND;
}
public void setIsDND(Boolean isDND) {
    this.isDND = isDND;
}
public Long getLastUpdatedAt() {
    return lastUpdatedAt;
}
public void setLastUpdatedAt(Long lastUpdatedAt) {
    this.lastUpdatedAt = lastUpdatedAt;
}
public String getLastUpdatedBy() {
    return lastUpdatedBy;
}
public void setLastUpdatedBy(String lastUpdatedBy) {
    this.lastUpdatedBy = lastUpdatedBy;
}
public List<Event> getEvents() {
    return events;
}
public void setEvents(List<Event> events) {
    this.events = events;
}

public Boolean getIsDeleted() {
    return isDeleted;
}
public void setIsDeleted(Boolean isDeleted) {
    this.isDeleted = isDeleted;
}
public String getCreatedBy() {
    return createdBy;
}
public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
}
public Long getCreatedAt() {
    return createdAt;
}
public void setCreatedAt(Long createdAt) {
    this.createdAt = createdAt;
}


}
