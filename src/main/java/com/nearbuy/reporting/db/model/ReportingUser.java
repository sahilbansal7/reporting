package com.nearbuy.reporting.db.model;

import java.io.Serializable;
import java.util.HashMap;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "reporting_user")
public class ReportingUser implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    @Id
    private String _id;
    private String emailId;
    private Long userId;
    private String date;
    private String templateId;
    private HashMap<String,String> placeHolderMap;
    
    public String get_id() {
        return _id;
    }
    public void set_id(String _id) {
        this._id = _id;
    }
    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getTemplateId() {
        return templateId;
    }
    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }
    public HashMap<String, String> getPlaceHolderMap() {
        return placeHolderMap;
    }
    public void setPlaceHolderMap(HashMap<String, String> placeHolderMap) {
        this.placeHolderMap = placeHolderMap;
    }
    public String getEmailId() {
        return emailId;
    }
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

}
