package com.nearbuy.reporting.db.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.mongodb.BasicDBObject;
import com.nearbuy.reporting.db.model.UserPreference;
import com.nearbuy.reporting.db.repository.UserPreferenceRepositoryCustom;
import com.nearbuy.reporting.model.Event;
import com.nearbuy.reporting.model.NotificationSetting;

public class UserPreferenceRepositoryImpl implements UserPreferenceRepositoryCustom {
    

   @Autowired
    private MongoOperations mongoOperations; 

    @Override
    public boolean deleteEventFromUserPreference(String userType, Long userId, String eventName,String updatedBy) {
        final Query query = new Query();
        Update update = new Update();
        query.addCriteria(Criteria.where("userType").is(userType));
        query.addCriteria(Criteria.where("userId").is(userId));
        query.addCriteria(Criteria.where("isDeleted").is(false));
        update.set("lastUpdatedAt", System.currentTimeMillis());
        update.set("lastUpdatedBy", updatedBy);
        update.pull("events", new BasicDBObject("eventName", eventName));
        mongoOperations.updateFirst(query, update, UserPreference.class);
        return true;
    }

    @Override
    public boolean editEventFromUserPreference(String userType, Long userId, Event event,String updatedBy) {
        final Query query = new Query();
        Update update = new Update();
        query.addCriteria(Criteria.where("userType").is(userType));
        query.addCriteria(Criteria.where("userId").is(userId));
        query.addCriteria(Criteria.where("isDeleted").is(false));
        update.set("lastUpdatedAt", System.currentTimeMillis());
        update.set("lastUpdatedBy", updatedBy);
        update.pull("events", new BasicDBObject("eventName", event.getEventName()));
        update.push("events", event);
        mongoOperations.updateFirst(query, update, UserPreference.class);
        update.push("events", event);
        mongoOperations.updateFirst(query, update, UserPreference.class);
        return true;
    }

    @Override
    public boolean addEventFromUserPreference(String userType, Long userId, Event event,String updatedBy) {
        final Query query = new Query();
        Update update = new Update();
        query.addCriteria(Criteria.where("userType").is(userType));
        query.addCriteria(Criteria.where("userId").is(userId));
        query.addCriteria(Criteria.where("isDeleted").is(false));
        update.set("lastUpdatedAt", System.currentTimeMillis());
        update.set("lastUpdatedBy", updatedBy);
        update.push("events", event);
        mongoOperations.updateFirst(query, update, UserPreference.class);
        return true;
    }

    @Override
    public boolean deleteNotificationSettingForEvent(String userType, Long userId, String eventName, NotificationSetting notificationSetting, String updatedBy,String notificationType) {
        final Query query = new Query();
        Update update = new Update();
        query.addCriteria(Criteria.where("userType").is(userType));
        query.addCriteria(Criteria.where("userId").is(userId));
        query.addCriteria(Criteria.where("isDeleted").is(false));
        query.addCriteria(Criteria.where("events.eventName").is(eventName));
        update.set("lastUpdatedAt", System.currentTimeMillis());
        update.set("lastUpdatedBy", updatedBy);
        update.pull("events.$.notificationSettings", new BasicDBObject("notificationType", notificationType));
       // update.push("events.$.notificationSettings", notificationSetting);
        mongoOperations.updateFirst(query, update, UserPreference.class);
        return true;
    }

    @Override
    public boolean addNotificationSettingForEvent(String userType, Long userId, String eventName, NotificationSetting notificationSetting, String updatedBy) {
        final Query query = new Query();
        Update update = new Update();
        query.addCriteria(Criteria.where("userType").is(userType));
        query.addCriteria(Criteria.where("userId").is(userId));
        query.addCriteria(Criteria.where("isDeleted").is(false));
        query.addCriteria(Criteria.where("events.eventName").is(eventName));
        update.set("lastUpdatedAt", System.currentTimeMillis());
        update.set("lastUpdatedBy", updatedBy);
        update.push("events.$.notificationSettings", notificationSetting);
        mongoOperations.updateFirst(query, update, UserPreference.class);
        return true;
    }
    
  

}
