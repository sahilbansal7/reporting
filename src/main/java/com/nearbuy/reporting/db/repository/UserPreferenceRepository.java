package com.nearbuy.reporting.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.nearbuy.reporting.db.model.UserPreference;
import com.nearbuy.reporting.model.NotificationSetting;
public interface UserPreferenceRepository extends  MongoRepository<UserPreference, String>,UserPreferenceRepositoryCustom  {

    @Query("{userType : ?0 , 'userId' : ?1,'isDeleted' : false}")
    public UserPreference findUserbyUserTypeanduserId(String userType, Long userId);
    
    @Query("{userType : ?0 , 'userId' : ?1,'events':{$elemMatch:{eventName:?2}},'isDeleted' : false}")
    public UserPreference findUserbyUserTypeUserIdAndEventName(String userType, Long userId,String eventName);
    
    @Query(value = "{'events':{$elemMatch:{eventName:?0}}, 'userType' : ?1,'isDeleted' : false}")
    public List<UserPreference> findUsersbyEventTypeAndUserType(String eventName,String userType);
    
    @Query(value = "{'events.notificationSettings':{$elemMatch:{notificationType:?0}}, 'userType' : ?1,'isDeleted' : false}")
    public List<UserPreference> findUsersbyNotificationTypeAndUserType(String notificationType,String userType);
   
}
