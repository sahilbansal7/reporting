package com.nearbuy.reporting.db.repository;

import com.nearbuy.reporting.model.Event;
import com.nearbuy.reporting.model.NotificationSetting;


public interface UserPreferenceRepositoryCustom {
    public boolean deleteEventFromUserPreference(String userType, Long userId, String eventName,String updatedBy);

    public boolean editEventFromUserPreference(String userType, Long userId, Event event,String updatedBy);

    public boolean addEventFromUserPreference(String userType, Long userId, Event event,String updatedBy);
    
    public boolean deleteNotificationSettingForEvent(String userType, Long userId,String eventName, NotificationSetting notificationSetting,String updatedBy,String notificationType);

    public boolean addNotificationSettingForEvent(String userType, Long userId,String eventName, NotificationSetting notificationSetting,String updatedBy);

}
