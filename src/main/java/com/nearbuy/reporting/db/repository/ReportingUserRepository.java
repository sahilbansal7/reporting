package com.nearbuy.reporting.db.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.nearbuy.reporting.db.model.ReportingUser;
public interface ReportingUserRepository extends PagingAndSortingRepository<ReportingUser, String> {
    @Query("{date : ?0}")
    List<ReportingUser> findUsersByDate(String date);
    
}
