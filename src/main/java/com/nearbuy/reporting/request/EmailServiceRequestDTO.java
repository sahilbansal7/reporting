package com.nearbuy.reporting.request;

import java.util.List;

import org.springframework.core.io.Resource;

public class EmailServiceRequestDTO {

	private List<String> emailTo;
	private List<String> ccTo;
	private List<String> bccTo;
	private String emailfrom;
	private String subject;
	private String emailBody;
	private List<Resource> attachments;

	public List<String> getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(List<String> emailTo) {
		this.emailTo = emailTo;
	}

	public List<String> getCcTo() {
		return ccTo;
	}

	public void setCcTo(List<String> ccTo) {
		this.ccTo = ccTo;
	}

	public List<String> getBccTo() {
		return bccTo;
	}

	public void setBccTo(List<String> bccTo) {
		this.bccTo = bccTo;
	}

	public String getEmailfrom() {
		return emailfrom;
	}

	public void setEmailfrom(String emailfrom) {
		this.emailfrom = emailfrom;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getEmailBody() {
		return emailBody;
	}

	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

	public List<Resource> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<Resource> attachments) {
		this.attachments = attachments;
	}
}
