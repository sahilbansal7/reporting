package com.nearbuy.reporting.request;

import java.util.List;

import com.nearbuy.reporting.model.Event;

public class UpdateEventsRequest {
    
    private  String updatedBy;
    private List<Event> events;

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    
    
}
