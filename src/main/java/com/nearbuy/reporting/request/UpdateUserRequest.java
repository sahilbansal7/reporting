package com.nearbuy.reporting.request;


public class UpdateUserRequest {

    private Boolean isDND;
    private String updatedBy;
 
    public Boolean getIsDND() {
        return isDND;
    }
    public void setIsDND(Boolean isDND) {
        this.isDND = isDND;
    }
    public String getUpdatedBy() {
        return updatedBy;
    }
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    
}
