package com.nearbuy.reporting.request;

import java.util.List;
import java.util.Map;

public class TemplateEmailRequest {

    private String templateId;

    private boolean isHtml;

    private Map<String, String> placeHolderMap;

    private List<String> ccTo;

    private List<String> emailTo;

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public boolean getIsHtml() {
        return isHtml;
    }

    public void setIsHtml(boolean isHtml) {
        this.isHtml = isHtml;
    }

    public Map<String,String> getPlaceHolderMap() {
        return placeHolderMap;
    }

    public void setPlaceHolderMap(Map<String, String> placeHolderMap) {
        this.placeHolderMap = placeHolderMap;
    }

    public List<String> getCcTo() {
        return ccTo;
    }

    public void setCcTo(List<String> ccTo) {
        this.ccTo = ccTo;
    }

    public List<String> getEmailTo() {
        return emailTo;
    }

    public void setEmailTo(List<String> emailTo) {
        this.emailTo = emailTo;
    }

    @Override
    public String toString() {
        return "TemplateEmailRequest [templateId=" + templateId + ", ccTo=" + ccTo + ", emailTo=" + emailTo + "]";
    }


}
